package luisrrleal.com.first.course.app.firstspringapp.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserController {

    @GetMapping("/details")
    public String details(Model model) {
        model.addAttribute("sayHi", "Hello World");
        model.addAttribute("name", "Luis");
        model.addAttribute("lastName", "Leal");
        return "details";
    }
}
