package luisrrleal.com.first.course.app.firstspringapp.Controllers;

import java.util.HashMap;
import java.util.Map;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import luisrrleal.com.first.course.app.firstspringapp.models.User;

@RequestMapping("/api")
@RestController
public class UserRestController {

    @GetMapping("/rest-details")
    public Map<String, Object> details() {
        Map<String, Object> resp = new HashMap<>();

        User user = new User("Luis", "Leal");

        resp.put("sayHi", "Hello World");
        resp.put("name", "Luis");
        resp.put("lastName", "Leal");
        resp.put("user", user);
        return resp;
    }
}
